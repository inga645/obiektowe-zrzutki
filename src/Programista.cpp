#include "Programista.h"
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

Programista::Programista(string x, int y, float z){
    nazwa = x;
    stanKonta = z;
    id = y;
}

Programista::Programista(string x, int y){
    nazwa = x;
    stanKonta = 0.0;
    id = y;
}

Programista::~Programista()
{
    //dtor
}

void Programista::operator+=(float x){
    stanKonta += x;
}

void Programista::operator-=(float x){
    if(stanKonta < x) throw 5;
    stanKonta -= x;
}

float Programista::podajCeneZamowienia(){
    float kwota;
    cout << "Podaj cene zamowienia dla programisty " << nazwa << ": ";

    try{
        cin >> kwota;
        if(cin.fail()) throw 3;
    }
    catch(int &err){
        cout << "Kwota jest liczba" << endl;
        cin.clear();
        cin.sync();
        return -1;
    }
    return kwota;
}

float Programista::kwotaDoZrzuty(){
    float skladka;
    cout << "Podaj wysokosc skladki ";
    try{
        cin >> skladka;
        if(cin.fail()) throw 3;
    }
    catch(int &err){
        cout << "Kwota jest liczba" << endl;
        cin.clear();
        cin.sync();
        return -1;
    }
    return skladka;
}

int Programista::oddanaReszta(){
    float reszta;
    cout << "Ile reszty w gotowce otrzymal/a " << nazwa << "? ";
    try{
        cin >> reszta;
        if(cin.fail()) throw 3;
    }
    catch(int &err){
        cout << "Kwota jest liczba" << endl;
        cin.clear();
        cin.sync();
        return -1;
    }
    zmienStanKonta(reszta);
    return 0;
}

int Programista::getId(){
    return id;
}

char Programista::getTyp(){
    return typ;
}

float Programista::getStan(){
    return stanKonta;
}

string Programista::getNazwa(){
    return nazwa;
}

void Programista::zmienStanKonta(float x){
    if(stanKonta + x < 0) throw 5;
    stanKonta += x;
}

void Programista::wyslijPrzelew(float x){
    throw 1;
}

void Programista::odbierzPrzelew(float x){
    throw 2;
}
