#include "Operacja.h"
#include "Programista.h"
#include <iostream>

using namespace std;

Operacja::Operacja(){
}

Operacja::~Operacja()
{
}

int Operacja::zbierzZamowienie(Kasa<Programista> l){
    int t;
    cout << "1. Dodaj nastepne zamowienie\n2. Zakoncz tworzenie listy zamowien" << endl;
    try{
        cin >> t;
        if(cin.fail()) throw 3;
    }
    catch(int &err){
        cout << "Podaj cyfre 1 lub 0" << endl;
        cin.clear();
        cin.sync();
    }

    while(t==1){
        listaZamawiajacych.push_back(order());
        int id;
        float cena, kwota, zwrot;
        cout << "Podaj ID programisty: ";
        try{
            cin >> id;
            if(cin.fail()) throw 3;
        }
        catch(int &err){
            cout << "ID jest liczba naturalna" << endl;
            cin.clear();
            cin.sync();
            continue;
        }

        for(unsigned int i=0; i < l.listaPracownikow.size(); i++){
            if(l.listaPracownikow[i]->getId() == id){
                Programista p = *l.listaPracownikow[i];
                cena = p.podajCeneZamowienia();
                    if(cena == -1) return 1;
                kwota = p.kwotaDoZrzuty();
                    if(kwota == -1) return 1;
                try{
                    zwrot = kwota - cena;
                    if((l.listaPracownikow[i]->getStan() + zwrot) < 0)
                        throw 5;
                    *(l.listaPracownikow[i])+=zwrot;
                }
                catch(int &err){
                    cout << "Niewystarczajaca suma srodkow na koncie" << endl;
                }
                break;
            }
        }
        listaZamawiajacych.back().id = id;
        listaZamawiajacych.back().cena = cena;
        listaZamawiajacych.back().zrzuta = kwota;
        cout << "\n1. Dodaj nastepne zamowienie\n2. Zakoncz tworzenie listy zamowien" << endl;
        try{
            cin >> t;
            if(cin.fail()) throw 3;
        }
        catch(int &err){
            cout << "Podaj cyfre 1 lub 2" << endl;
            cin.clear();
            cin.sync();
        }
    }

    return 0;
}
