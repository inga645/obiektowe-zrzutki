#include "Program.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
using namespace std;

Program::Program(){
    //ctor
}

Program::~Program(){
    //dtor
}

void Program::wczytajStan(Kasa<Programista> &l){
    fstream plik;
    string osoba[4];

    plik.open("dane.txt", ios::in);

    if(plik.good()==true){
            int konta;
            plik >> konta;
            l.ustawKonta(konta);

        while(plik >> osoba[0]){
            for(int i = 1; i < 4; i++)
                plik >> osoba[i];

            stringstream tmp;
            tmp << osoba[1] << " " << osoba[3];
            int x;
            float y;
            tmp >> x;
            tmp >> y;

            if(osoba[0][0] == 'p'){
                Pracownik *p = new Pracownik(osoba[2],x,y);
                l.listaPracownikow.push_back(p);
            }
            else if(osoba[0][0] == 's'){
                Stazysta *p = new Stazysta(osoba[2],x,y);
                l.listaPracownikow.push_back(p);
            }
        }
        plik.close();
    }
}

void Program::zapiszStan(Kasa<Programista> l){
    fstream plik;
    plik.open("dane.txt", ios::out | ios::trunc);
    if(plik.good()){
            plik << l.ileKont() << endl;
            for(unsigned int i=0; i<l.listaPracownikow.size(); i++){
                char t = l.listaPracownikow[i]->getTyp();
                int x = l.listaPracownikow[i]->getId();
                string n = l.listaPracownikow[i]->getNazwa();
                float s = l.listaPracownikow[i]->getStan();
                plik << t << " " << x << " " << n << " " << s <<endl;
                    //typ - id - nazwa - stan
            }
    }
    plik.close();
}
