#include "Pracownik.h"
#include <iostream>
#include <vector>

using namespace std;

Pracownik::Pracownik(string nazwa, int id, float stanKonta) : Programista(nazwa, id, stanKonta){
    typ = 'p';
}

Pracownik::Pracownik(string nazwa, int id) : Programista(nazwa, id){
    typ = 'p';
}

Pracownik::~Pracownik()
{
    //dtor
}

void Pracownik::wyslijPrzelew(float kwota){
    if(stanKonta < kwota) throw 5;
    stanKonta -= kwota;
}

void Pracownik::odbierzPrzelew(float kwota){
    stanKonta += kwota;
}
