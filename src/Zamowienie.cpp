#include "Zamowienie.h"
#include <iostream>

using namespace std;

Zamowienie::Zamowienie()
{
    //ctor
}

Zamowienie::~Zamowienie()
{
    //dtor
}

void Zamowienie::oddajReszte(Kasa<Programista> l)
{
    for(unsigned int i = 0; i < listaZamawiajacych.size(); i++){
        for(unsigned int j = 0; j < l.listaPracownikow.size(); j++){
            if(listaZamawiajacych[i].id == l.listaPracownikow[j]->getId())
                l.listaPracownikow[j]->oddanaReszta();
        }
    }
}
