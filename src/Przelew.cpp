#include "Przelew.h"
#include <iostream>

using namespace std;

Przelew::Przelew() : Operacja(){
}

Przelew::~Przelew()
{
    //dtor
}

int Przelew::zbierzZamowienie(){
    int id1, id2;
    float kwota;

    cout << "Podaj ID konta 1: ";
    try{
        cin >> id1;
        if(cin.fail()) throw 4;
    }
    catch(int &err){
        cout << "ID jest liczba naturalna!" << endl;
        cin.clear();
        cin.sync();
        return 4;
    }
    idWysylajacego = id1;

    cout << "Podaj ID konta 2: ";
    try{
        cin >> id2;
        if(cin.fail()) throw 4;
    }
    catch(int &err){
        cout << "ID jest liczba naturalna!" << endl;
        cin.clear();
        cin.sync();
        return 4;
    }
    idOdbierajacego = id2;

    cout << "Podaj kwote przelewu: ";
    try{
        cin >> kwota;
        if(cin.fail()) throw 3;
    }
    catch(int &err){
        cout << "Kwota jest liczba!" << endl;
        cin.clear();
        cin.sync();
        return 3;
    }
    kwotaPrzelewu = kwota;
    return 0;
}

int Przelew::przelej(Kasa<Programista> l){
    int x = zbierzZamowienie();
    if(x==3 || x==4)
        return 1;


    int wys, odb;

    for(unsigned int i=0; i<l.listaPracownikow.size(); i++){
        if(l.listaPracownikow[i]->getId()==idWysylajacego){
            wys = i;
            continue;
        }
        if(l.listaPracownikow[i]->getId()==idOdbierajacego){
            odb = i;
            continue;
        }
    }

    try{
        l.listaPracownikow[wys]->wyslijPrzelew(kwotaPrzelewu);
        l.listaPracownikow[odb]->odbierzPrzelew(kwotaPrzelewu);
    }
    catch(int &ex){
        //cerr << ex;
        if(ex == 1)
            cout << "Przelew moze wyslac tylko pracownik!" << endl;
        else if(ex == 2){
            l.listaPracownikow[wys]->zmienStanKonta(kwotaPrzelewu);
            cout << "Przelew moze odebrac tylko pracownik!" << endl;
        }
        else if(ex == 5)
            cout << "Niewystaraczajaca suma srodkow na koncie" << endl;
    }
    return 0;
}
