#ifndef KASA_H
#define KASA_H
#include "Pracownik.h"
#include "Stazysta.h"
#include <vector>
#include <iostream>

template <class T>
class Kasa
{
        float saldo;
        int konta;
    public:
        std::vector<T*> listaPracownikow;

        Kasa(){
            saldo = 0.0;
            konta = 0;
        }

        int ileKont(){
            return konta;
        }

        void ustawKonta(int x){
            konta = x;
        }

        int dodajKonto(){
            int i;
            std::string nazwa;
            std::cout << "Wybierz typ konta (pracownik-1/stazysta-2) ";
            try{
                std::cin >> i;
                if(std::cin.fail()) throw 3;
            }
            catch(int &err){
                std::cout << "Podaj cyfre 1 lub 0" << std::endl;
                std::cin.clear();
                std::cin.sync();
                return 1;
            }
            std::cout << "Podaj nazwe konta (bez spacji - w razie wpisania spacji nazwa jest pierwszy czlon): ";
            std::cin >> nazwa;

            if(i==1){
                T *p = new Pracownik(nazwa,konta+1);
                listaPracownikow.push_back(p);
            }
            else if(i==2){
                T *p = new Stazysta(nazwa,konta+1);
                listaPracownikow.push_back(p);
            }

            konta++;
            return 0;
        }

        void usunKonto(){
            int id;
            std::cout << "Podaj ID konta do usuniecia: ";
            try{
                std::cin >> id;
                if(std::cin.fail()) throw 3;
            }
            catch(int &err){
                std::cout << "ID jest liczba naturalna" << std::endl;
                std::cin.clear();
                std::cin.sync();
            }

            for(unsigned int i = 0; i < listaPracownikow.size(); i++){
                if(listaPracownikow[i]->getId() == id){
                        if(listaPracownikow[i]->getStan() == 0){
                            listaPracownikow.erase(listaPracownikow.begin()+i);
                        }
                        else
                            std::cout << "Nie mozna usunac konta z niezerowym saldem!" << std::endl;
                    break;
                }
            }
        }

        void wyswietlSaldo(){
            saldo = 0;
            for(unsigned int i = 0; i<listaPracownikow.size(); i++)
                saldo += listaPracownikow[i]->getStan();
            std::cout << "Stan kasy: " << saldo << std::endl;
        }

        void wyswietlWszystko(){
            std::cout << "Stan kont programistow:" << std::endl;
            std::cout << "ID\tNAZWA\tSALDO\tTYP" << std::endl;

            for(unsigned int i = 0; i < listaPracownikow.size(); i++){
                int id = listaPracownikow[i]->getId();
                std::string n = listaPracownikow[i]->getNazwa();
                float s = listaPracownikow[i]->getStan();
                char t = listaPracownikow[i]->getTyp();
                std::cout << id << "\t" << n << "\t" << s << "\t" << t << std::endl;
            }
        }
        ~Kasa(){
            listaPracownikow.clear();
        };
};

#endif // KASA_H
