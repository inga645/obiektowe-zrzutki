#ifndef PROGRAM_H
#define PROGRAM_H
#include "Kasa.h"

class Program
{
    public:
        Program();
        void wczytajStan(Kasa<Programista>&);
        void zapiszStan(Kasa<Programista>);
        virtual ~Program();

};

#endif // PROGRAM_H
