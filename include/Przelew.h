#ifndef PRZELEW_H
#define PRZELEW_H

#include <Operacja.h>
#include "Kasa.h"

class Przelew : public Operacja
{
    int idWysylajacego, idOdbierajacego;
    float kwotaPrzelewu;
    public:
        Przelew();
        int zbierzZamowienie();
        int przelej(Kasa<Programista>);
        virtual ~Przelew();
};

#endif // PRZELEW_H
