#ifndef STAZYSTA_H
#define STAZYSTA_H

#include <Programista.h>

class Stazysta : public Programista
{
    public:
        Stazysta(std::string,int,float);
        Stazysta(std::string, int);
        virtual ~Stazysta();

};

#endif // STAZYSTA_H
