#ifndef PROGRAMISTA_H
#define PROGRAMISTA_H
#include <iostream>
//#include "Operacja.h"

class Programista
{
    protected:
        char typ;
        std::string nazwa;
        int id;
        float stanKonta;
    public:
        Programista(std::string, int);
        Programista(std::string,int,float);

        void operator+=(float);
        void operator-=(float);

        int getId();
        char getTyp();
        float getStan();
        std::string getNazwa();

        float podajCeneZamowienia();
        float kwotaDoZrzuty();
        int oddanaReszta();
        void zmienStanKonta(float);

        virtual void odbierzPrzelew(float);
        virtual void wyslijPrzelew(float);

        virtual ~Programista();
};

#endif // PROGRAMISTA_H
