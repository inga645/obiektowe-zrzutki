#ifndef OPERACJA_H
#define OPERACJA_H
#include <vector>
#include "Kasa.h"
#include <iostream>

class Operacja
{
    private:
        struct order {
            int id;
            float cena;
            float zrzuta;
        };

    protected:
        std::vector<order> listaZamawiajacych;
    public:
        Operacja();
        virtual int zbierzZamowienie(Kasa<Programista>);
        virtual ~Operacja();
};

#endif // OPERACJA_H
