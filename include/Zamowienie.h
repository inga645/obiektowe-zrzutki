#ifndef ZAMOWIENIE_H
#define ZAMOWIENIE_H

#include <Operacja.h>
#include "Kasa.h"

class Zamowienie : public Operacja
{
    float cena;
    float reszta;
    public:
        Zamowienie();
        void oddajReszte(Kasa<Programista>);
        virtual ~Zamowienie();
};

#endif // ZAMOWIENIE_H
