#ifndef PRACOWNIK_H
#define PRACOWNIK_H

#include <Programista.h>
#include <iostream>

class Pracownik : public Programista
{
    public:
        Pracownik(std::string, int, float);
        Pracownik(std::string, int);
        void wyslijPrzelew(float);
        void odbierzPrzelew(float);
        virtual ~Pracownik();
};

#endif // PRACOWNIK_H
