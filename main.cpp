#include "Kasa.h"
#include "Program.h"
#include "Programista.h"
#include "Pracownik.h"
#include "Stazysta.h"
#include "Operacja.h"
#include "Przelew.h"
#include "Zamowienie.h"

#include <iostream>

using namespace std;

void wyswietlMenu(){
    cout << "\n\nMENU" << endl;
    cout << "1 - Dodaj konto" << endl;
    cout << "2 - Usun konto" << endl;
    cout << "3 - Edytuj saldo konta" << endl;
    cout << "4 - Wykonaj przelew miedzy kontami" << endl;
    cout << "5 - Wyswietl stan kasy" << endl;
    cout << "6 - Wyswietl stan wszystkich kont" << endl;
    cout << "7 - Zloz zamowienie" << endl;
    cout << "8 - Wyjdz z programu\n" << endl;
}

int main()
{
    int wybor;
    Program app;
    Kasa<Programista> kasa;
    Przelew przelew;
    Zamowienie zamow;

    app.wczytajStan(kasa);

    do{
        wyswietlMenu();
        try{
            cin >> wybor;
            if(cin.fail()) throw 3;
        }
        catch(int &err){
            cout << "Podaj cyfre z zakresu 1-8" << endl;
            cin.clear();
            cin.sync();
            continue;
        }

        switch(wybor){
        case 1:
            kasa.dodajKonto();
            break;
        case 2:
            kasa.usunKonto();
            break;

        case 3:
            int id;
            float kwota;
            cout << "Podaj id programisty: ";

            try{
                cin >> id;
                if(cin.fail()) throw 3;
            }
            catch(int &err){
                cout << "ID jest liczba naturalna!" << endl;
                cin.clear();
                cin.sync();
            }
            for(unsigned int i = 0; i < kasa.listaPracownikow.size(); i++){
                if(kasa.listaPracownikow[i]->getId() == id){
                    cout << "Podaj kwote wplaty (przy wyplacie dopisz - ): ";

                    try{
                        cin >> kwota;
                        if(cin.fail()) throw 3;
                    }
                    catch(int &err){
                        cout << "Kwota jest liczba!" << endl;
                        cin.clear();
                        cin.sync();
                    }
                    try{
                        kasa.listaPracownikow[i]->zmienStanKonta(kwota);
                    }
                    catch(int &err){
                        cout << "Niewystarczajaca suma srodkow na koncie" << endl;
                    }
                    break;
                }
            }
            break;

        case 4:
            przelew.przelej(kasa);
            break;
        case 5:
            kasa.wyswietlSaldo();
            break;
        case 6:
            kasa.wyswietlWszystko();
            break;
        case 7:
            zamow.zbierzZamowienie(kasa);
        case 8:
            break;
        default:
            cout << "Nie wybrano zadnej opcji" << endl;
        }
    }
    while(wybor != 8);

    app.zapiszStan(kasa);
    return 0;
}
